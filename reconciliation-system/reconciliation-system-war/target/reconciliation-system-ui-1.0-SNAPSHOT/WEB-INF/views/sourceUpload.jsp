<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Source Upload</title>

    <style>
        body {
            font-family: Arial, Helvetica, sans-serif;
        }

        .container {
            margin: auto;
            width: 70%;
            padding: 10px;
        }

        .field {
            margin: auto;
            width: 60%;
            border-color: black;
            border-style: solid;
            border-width: medium;
        }

        .btn-primary {
            width: 20%;
            padding: 12px;
            border-style: solid;
            border-width: thin;
            border-color: black;
            border-radius: 10px;
            color: #f2f2f2;
            background: blue;
            margin-top: 15px;
            margin-bottom: 15px;
            margin-left: 300px;
            opacity: 0.85;
            display: inline-block;
            font-size: 17px;
            line-height: 20px;
        }

        .input {
            border: black;
            border-radius: 4px;
            margin-top: 15px;
            margin-bottom: 15px;
            opacity: 0.85;
            display: inline-block;
            font-size: 17px;
            line-height: 20px;
        }
    </style>
</head>
<body>
<form method="post" action="${pageContext.request.contextPath}/sourceUpload"  enctype="multipart/form-data">
    <div class="container">
    <div>
        <p>Source Name:</p>
        <input type="text" name="sourceName" class="field"/>
    </div>
    <div>
        <p>File Type:</p>
        <select name="type" id="type" class="field">
            <option value="csv">CSV</option>
            <option value="json">Json</option>
        </select>
    </div>
    <div>
        <p>Select Source file to upload:</p>
        <input type="file" name="sourceFile" id="fileChooser" class="input"/>
    </div>
    <button type="submit" class="btn-primary">Submit</button>
    </div>
</form>

</body>
</html>
