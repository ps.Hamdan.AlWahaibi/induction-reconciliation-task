<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Compare</title>

    <style>
        .container {
            margin: auto;
            width: 70%;
            padding: 10px;
        }

        .sourceBox {
            width: 30%;
            margin-top: 15px;
            margin-bottom: 15px;
            margin-left: 15px;
            border-style: solid;
            border-width: medium;
            border-color: #2f79c6;
            border-radius: 25px;
            text-align: center;
            color: #7dabc1;
        }


        .targetBox {
            width: 30%;
            margin-top: 15px;
            margin-bottom: 15px;
            margin-left: 15px;
            border-style: solid;
            border-width: medium;
            border-color: #7dabc1;
            border-radius: 25px;
            text-align: center;
            color: #2f79c6;
        }

        .btn-primary {
            width: 20%;
            padding: 12px;
            border-style: solid;
            border-width: thin;
            border-color: black;
            border-radius: 10px;
            color: #f2f2f2;
            background: blue;
            margin-top: 15px;
            margin-bottom: 15px;
            opacity: 0.85;
            display: inline-block;
            font-size: 17px;
            line-height: 20px;
        }

        .btn {
            width: 20%;
            padding: 12px;
            border-style: solid;
            border-width: thin;
            border-color: black;
            border-radius: 10px;
            margin: 5px 0;
            opacity: 0.85;
            display: inline-block;
            font-size: 17px;
            line-height: 20px;
            text-decoration: none;
        }
    </style>
</head>
<body>
<form method="post" action="${pageContext.request.contextPath}/compare" enctype="multipart/form-data">
    <div class="container">
        <div class="sourceBox">
            <h3>Source:</h3>
            <h4>File Name: ${sessionScope.sourceName}</h4>
            <h4>File Type: ${sessionScope.sourceType}</h4>
        </div>
        <div class="targetBox">
            <h3>Target:</h3>
            <h4>File Name: ${sessionScope.targetName}</h4>
            <h4>File Type: ${sessionScope.targetType}</h4>

        </div>
        <div>
            <input class="btn" type="button"
                   onclick="window.location.replace('http://localhost:8080/reconciliation-system/sourceUpload')"
                   value="Cancel"/>
            <input class="btn" type=button value="Back" onCLick="history.back()">
            <input class="btn-primary" type="submit" value="Compare">
        </div>
    </div>
</form>

</body>
</html>
