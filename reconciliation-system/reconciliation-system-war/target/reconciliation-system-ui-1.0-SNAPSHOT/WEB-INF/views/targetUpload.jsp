<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Target Upload</title>
    <style>
        body {
            font-family: Arial, Helvetica, sans-serif;
        }

        .container {
            margin: auto;
            width: 70%;
            padding: 10px;
        }

        .field {
            margin: auto;
            width: 60%;
            border-color: black;
            border-style: solid;
            border-width: medium;
        }

        .btn-primary {
            width: 20%;
            padding: 12px;
            border-style: solid;
            border-width: thin;
            border-color: black;
            border-radius: 10px;
            color: #f2f2f2;
            background: blue;
            margin-top: 15px;
            margin-bottom: 15px;
            margin-left: 300px;
            opacity: 0.85;
            display: inline-block;
            font-size: 17px;
            line-height: 20px;
        }

        .input {
            border: black;
            border-radius: 4px;
            margin-top: 15px;
            margin-bottom: 15px;
            opacity: 0.85;
            display: inline-block;
            font-size: 17px;
            line-height: 20px;
        }

        .btn {
            width: 20%;
            padding: 12px;
            border-style: solid;
            border-width: thin;
            border-color: black;
            border-radius: 10px;
            margin: 5px 0;
            opacity: 0.85;
            display: inline-block;
            font-size: 17px;
            line-height: 20px;
            text-decoration: none;
        }
    </style>
</head>
<body>
<form method="post" action="${pageContext.request.contextPath}/targetUpload" enctype="multipart/form-data">
    <div class="container">
        <div>
            <p>Target Name:</p>
            <input class="field" type="text" name="targetName"/>
        </div>
        <div>
            <p>File Type:</p>
            <select class="field" name="type" id="type" style="padding-left: 15px">
                <option value="csv">CSV</option>
                <option value="json">Json</option>
            </select>
        </div>
        <div>
            <p>Select Target file to upload:</p>
            <input class="input" type="file" name="targetFile" id="fileChooser"/>
        </div>
        <input class="btn" type=button value="Back" onCLick="history.back()">
        <button type="submit" class="btn-primary">Submit</button>
    </div>
</form>
</body>
</html>
