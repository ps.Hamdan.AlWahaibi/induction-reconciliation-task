<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Results</title>
    <style>
        body {
            margin: 0;
            font-family: Arial, Helvetica, sans-serif;
        }

        .topnav {
            overflow: hidden;
        }

        .topnav a {
            float: left;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
            font-size: 17px;
        }

        .topnav a:hover {
            color: black;
        }

        .topnav a.active {
            color: blue;
        }
    </style>
</head>
<body>
<form method="post" action="${pageContext.request.contextPath}/result" enctype="multipart/form-data">
    <div class="topnav">
        <a class="active" href="#match">Match</a>
        <a href="#missmatch">Mismatch</a>
        <a href="#missing">Missing</a>
    </div>


    <input type="submit" value="Login" class="btn">
</form>

</body>
</html>
