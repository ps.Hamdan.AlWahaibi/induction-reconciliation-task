<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login Page</title>
    <style>

        body {
            font-family: Arial, Helvetica, sans-serif;
        }

        .container {
            margin: auto;
            width: 70%;
            padding: 10px;
        }

        .field {
            margin: auto;
            width: 60%;
            border-color: black;
            border-style: solid;
            border-width: medium;
        }

        input,
        .btn {
            width: 20%;
            padding: 12px;
            border-style: solid;
            border-width: thin;
            border-color: black;
            border-radius: 10px;
            margin: 5px 0;
            opacity: 0.85;
            display: inline-block;
            font-size: 17px;
            line-height: 20px;
            text-decoration: none;
        }


        input[type=submit] {
            background-color: darkblue;
            border-style: solid;
            border-width: thin;
            border-color: black;
            border-radius: 10px;
            color: white;
            cursor: pointer;

        }


    </style>
</head>

<body>
<div class="container">
    <form action="${pageContext.request.contextPath}/login" method="post" >
        <div>
            <div>
                <p>Username</p>
                <input type="text" name="username" class="field">
            </div>
            <div>
                <p>Password</p>
                <input type="password" name="password" class="field">
            </div>
            <div>
                <input type="submit" value="Login" class="btn">
            </div>
        </div>
    </form>
    <p>${message}</p>
</div>
</body>
</html>