package com.progressoft.jip9.Servlets;

import com.progressoft.jip9.Comparator.FileInfo;
import com.progressoft.jip9.Comparator.FileType;
import com.progressoft.jip9.Comparator.FilesComparator;
import com.progressoft.jip9.Exceptions.FileException;
import com.progressoft.jip9.Exceptions.JSONParseException;
import com.progressoft.jip9.Readers.TransactionRecord;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

@WebServlet(urlPatterns = "/compare")
public class CompareServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
// TODO static paths
        Path srcFilePath = Paths.get("/home/oman/Desktop/new/induction-reconciliation-task/reconciliation-system/reconciliation-system-war/target/tmp/uploads/source");
        Path targetFilePath = Paths.get("/home/oman/Desktop/new/induction-reconciliation-task/reconciliation-system/reconciliation-system-war/target/tmp/uploads/target");
        String sourceType = (String) request.getSession().getAttribute("sourceType");
        String targetType = (String) request.getSession().getAttribute("targetType");


        FileType srcFileType = selectFileType(sourceType);
        FileType targetFileType = selectFileType(targetType);

        FileInfo source = new FileInfo(srcFilePath, srcFileType);
        FileInfo target = new FileInfo(targetFilePath, targetFileType);

        FilesComparator comparator = new FilesComparator();

        List<Map<String, TransactionRecord>> listOfComparisons;
        try {
            listOfComparisons = comparator.compare(source, target);
        } catch (IOException | JSONParseException e) {
            throw new FileException("invalid file type");
        }

        request.getSession().setAttribute("listOfComparisons", listOfComparisons);

        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/result.jsp");
        dispatcher.forward(request, response);

    }

    private FileType selectFileType(String fileType) {
        if (fileType.toLowerCase().equals("json"))
            return FileType.JSON;
        else if (fileType.toLowerCase().equals("csv"))
            return FileType.CSV;
        else throw new IllegalArgumentException("invalid file type");
    }

}
