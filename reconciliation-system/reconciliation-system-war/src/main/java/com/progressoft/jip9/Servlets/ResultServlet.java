package com.progressoft.jip9.Servlets;

import com.progressoft.jip9.Exporter.ResultFilesExporter;
import com.progressoft.jip9.Readers.TransactionRecord;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


@WebServlet(urlPatterns = "/result")
public class ResultServlet extends HttpServlet {

    private final int BUFFER_SIZE = 1048;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        List<Map<String, TransactionRecord>> listOfComparisons = (LinkedList<Map<String, TransactionRecord>>)
                request.getSession().getAttribute("listOfComparisons");

        ResultFilesExporter exporter = new ResultFilesExporter();

        exporter.export(listOfComparisons);

        response.setContentType("text/plain");
        response.setHeader("Content-disposition", "attachment; filename=sample.txt");

        // TODO what should I do with this path on my machine, this should be configured
        String filePath = "/home/oman/Desktop/new/induction-reconciliation-task/reconciliation-system/reconciliation-system-files/matching-transactions.csv";
        try (InputStream in = request.getServletContext().getResourceAsStream(filePath);
             OutputStream out = response.getOutputStream()) {

            byte[] buffer = new byte[BUFFER_SIZE];

            int numBytesRead;
            while ((numBytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, numBytesRead);
                out.flush();
            }
        }
    }
}
