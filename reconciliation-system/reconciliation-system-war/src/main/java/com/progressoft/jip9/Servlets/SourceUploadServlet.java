package com.progressoft.jip9.Servlets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;


@WebServlet (urlPatterns = "/sourceUpload")
@MultipartConfig(location = "uploads", maxFileSize=1048576,
        maxRequestSize=1048576, fileSizeThreshold=262144)
public class SourceUploadServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String sourceFiletype = request.getParameter("type");
        String sourceName = request.getParameter("sourceName");
        Part sourceFile = request.getPart("sourceFile");
        // TODO multiple users and session would override each other work if they upload the same file name
        sourceFile.write("source");
        request.getSession().setAttribute("sourceType", sourceFiletype);
        request.getSession().setAttribute("sourceName", sourceName);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/targetUpload.jsp");
        dispatcher.forward(request, response);
    }
}
