package com.progressoft.jip9.Initializers;


import com.progressoft.jip9.Servlets.LoginServlet;
import com.progressoft.jip9.Servlets.SourceUploadServlet;
import com.progressoft.jip9.Servlets.TargetUploadServlet;

import javax.servlet.*;
import java.util.Set;

public class ServletInitializer implements ServletContainerInitializer {
    @Override
    public void onStartup(Set<Class<?>> set, ServletContext servletContext) throws ServletException {
        // TODO useless code!!!
//        registerLoginServlet(servletContext);
//        registerSourceUploadServlet(servletContext);
//        registerTargetUploadServlet(servletContext);
//        registerCompareServlet(servletContext);
    }

    private void registerLoginServlet(ServletContext servletContext) {
        LoginServlet loginServlet = new LoginServlet();
        ServletRegistration.Dynamic servletRegistration = servletContext.addServlet("loginServlet", loginServlet);
        servletRegistration.addMapping("/login");
    }

    private void registerSourceUploadServlet(ServletContext servletContext) {
        SourceUploadServlet sourceUploadServlet = new SourceUploadServlet();
        ServletRegistration.Dynamic servletRegistration = servletContext.addServlet("fileUploadServlet", sourceUploadServlet);
        servletRegistration.setMultipartConfig(
                new MultipartConfigElement
                        ("uploads", 1048576,
                                1048576, 262144));
        servletRegistration.addMapping("/sourceUpload");

    }

    private void registerTargetUploadServlet(ServletContext servletContext) {
        TargetUploadServlet targetUploadServlet = new TargetUploadServlet();
        ServletRegistration.Dynamic servletRegistration = servletContext.addServlet("fileUploadServlet", targetUploadServlet);
        servletRegistration.setMultipartConfig(
                new MultipartConfigElement
                        ("uploads", 1048576,
                                1048576, 262144));
        servletRegistration.addMapping("/targetUpload");
    }

    private void registerCompareServlet(ServletContext servletContext) {
        LoginServlet loginServlet = new LoginServlet();
        ServletRegistration.Dynamic servletRegistration = servletContext.addServlet("compareServlet", loginServlet);
        servletRegistration.addMapping("/compare");
    }
}
