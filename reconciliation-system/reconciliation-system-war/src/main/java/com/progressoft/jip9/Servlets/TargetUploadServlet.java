package com.progressoft.jip9.Servlets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;

@WebServlet (urlPatterns = "/targetUpload")
@MultipartConfig(location = "uploads", maxFileSize=1048576,
        maxRequestSize=1048576, fileSizeThreshold=262144)
public class TargetUploadServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String targetFiletype = req.getParameter("type");
        String targetName = req.getParameter("targetName");
        Part sourceFile = req.getPart("targetFile");
        // TODO duplicate code with the source upload
        sourceFile.write("target");
        req.getSession().setAttribute("targetType", targetFiletype);
        req.getSession().setAttribute("targetName", targetName);
        RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/views/compare.jsp");
        dispatcher.forward(req, resp);
    }
}
