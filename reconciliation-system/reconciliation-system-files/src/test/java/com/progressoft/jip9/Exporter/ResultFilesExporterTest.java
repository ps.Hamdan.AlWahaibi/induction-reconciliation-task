package com.progressoft.jip9.Exporter;

import com.progressoft.jip9.Comparator.FileInfo;
import com.progressoft.jip9.Comparator.FileType;
import com.progressoft.jip9.Comparator.FilesComparator;
import com.progressoft.jip9.Exceptions.JSONParseException;
import com.progressoft.jip9.Readers.TransactionRecord;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.apache.commons.io.FileUtils;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

public class ResultFilesExporterTest {
    @Test
    public void canCreate() {
        new ResultFilesExporter();
    }

    @Test
    public void givenValidListOfComparisons_whenExportResults_thenSuccess() throws IOException, JSONParseException {
        Path sourcePath = Files.createTempFile("transaction", ".csv");
        try (InputStream resourceAsStream =
                     this.getClass().getResourceAsStream("/bank-transactions.csv");
             OutputStream outputStream = Files.newOutputStream(sourcePath)) {
            int read;
            while ((read = resourceAsStream.read()) != -1)
                outputStream.write(read);
        }

        Path targetPath = Files.createTempFile("transaction", ".JSON");
        try (InputStream resourceAsStream =
                     this.getClass().getResourceAsStream("/online-banking-transactions.json");
             OutputStream outputStream = Files.newOutputStream(targetPath)) {
            int read;
            while ((read = resourceAsStream.read()) != -1)
                outputStream.write(read);
        }

        FileInfo source = new FileInfo(sourcePath, FileType.CSV);
        FileInfo target = new FileInfo(targetPath, FileType.JSON);

        FilesComparator comparator = new FilesComparator();
        List<Map<String, TransactionRecord>> listOfComparisons = comparator.compare(source, target);

        ResultFilesExporter exporter = new ResultFilesExporter();
        exporter.export(listOfComparisons);


        File actualMatchingFile = new File("." + "/" + "matching-transactions.csv");
        File expectedMatchingFile = new File("src/test/resources/matching-transactions.csv");

        String actualMatched = FileUtils.readFileToString(actualMatchingFile, "utf-8");
        String expectedMatched = FileUtils.readFileToString(expectedMatchingFile, "utf-8");

        Assertions.assertEquals(expectedMatched, actualMatched);

        File actualMismatchingFile = new File("." + "/" + "mismatching-transactions.csv");
        File expectedMismatchingFile = new File("src/test/resources/mismatched-transactions.csv");

        String actualMismatching = FileUtils.readFileToString(actualMismatchingFile, "utf-8");
        String expectedMismatching = FileUtils.readFileToString(expectedMismatchingFile, "utf-8");

        Assertions.assertEquals(actualMismatching, expectedMismatching);

        File actualMissingFile = new File("." + "/" + "missing-transactions.csv");
        File expectedMissingFile = new File("src/test/resources/missing-transactions.csv");

        String actualMissing = FileUtils.readFileToString(actualMissingFile, "utf-8");
        String expectedMissing = FileUtils.readFileToString(expectedMissingFile, "utf-8");

        Assertions.assertEquals(actualMissing, expectedMissing);
        //  where is the assertion that the file is exported
    }
}
