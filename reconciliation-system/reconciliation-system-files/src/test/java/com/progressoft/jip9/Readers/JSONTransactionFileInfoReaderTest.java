package com.progressoft.jip9.Readers;

import com.progressoft.jip9.Exceptions.JSONParseException;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class JSONTransactionFileInfoReaderTest {
    @Test
    public void canCreate() {
        new JSONTransactionFileReader();
    }

    @Test
    public void givenValidPath_whenRead_thenSuccess() throws IOException, JSONParseException {
        Path path = Files.createTempFile("transaction", ".JSON");
        try (InputStream resourceAsStream =
                     this.getClass().getResourceAsStream("/online-banking-transactions.json");
             OutputStream outputStream = Files.newOutputStream(path)) {
            int read;
            while ((read = resourceAsStream.read()) != -1)
                outputStream.write(read);
        }

        JSONTransactionFileReader reader = new JSONTransactionFileReader();
        List<TransactionRecord> records = new ArrayList<>();
        reader.read(path, records::add);

        Assertions.assertNotNull(records, "records are null");
        Assertions.assertEquals(records.size(), 7);

        TransactionRecord transactionRecord = records.get(0);
        Assertions.assertNotNull(transactionRecord);

        Assertions.assertEquals("2020-01-20", transactionRecord.getValueDate());
        Assertions.assertEquals("TR-47884222201", transactionRecord.getTransID());
        Assertions.assertEquals(new BigDecimal("140.00"), transactionRecord.getAmount());
        Assertions.assertEquals("USD", transactionRecord.getCurrency());
        Assertions.assertEquals("donation", transactionRecord.getPurpose());

        transactionRecord = records.get(1);
        Assertions.assertEquals("2020-02-03", transactionRecord.getValueDate());
        Assertions.assertEquals("TR-47884222205", transactionRecord.getTransID());
        Assertions.assertEquals(new BigDecimal("60.000"), transactionRecord.getAmount());
        Assertions.assertEquals("JOD", transactionRecord.getCurrency());
        Assertions.assertEquals("", transactionRecord.getPurpose());

        transactionRecord = records.get(2);
        Assertions.assertEquals("2020-02-10", transactionRecord.getValueDate());
        Assertions.assertEquals("TR-47884222206", transactionRecord.getTransID());
        Assertions.assertEquals(new BigDecimal("500.00"), transactionRecord.getAmount());
        Assertions.assertEquals("USD", transactionRecord.getCurrency());
        Assertions.assertEquals("general", transactionRecord.getPurpose());

        transactionRecord = records.get(3);
        Assertions.assertEquals("2020-01-22", transactionRecord.getValueDate());
        Assertions.assertEquals("TR-47884222202", transactionRecord.getTransID());
        Assertions.assertEquals(new BigDecimal("30.000"), transactionRecord.getAmount());
        Assertions.assertEquals("JOD", transactionRecord.getCurrency());
        Assertions.assertEquals("donation", transactionRecord.getPurpose());

        transactionRecord = records.get(4);
        Assertions.assertEquals("2020-02-14", transactionRecord.getValueDate());
        Assertions.assertEquals("TR-47884222217", transactionRecord.getTransID());
        Assertions.assertEquals(new BigDecimal("12000.000"), transactionRecord.getAmount());
        Assertions.assertEquals("JOD", transactionRecord.getCurrency());
        Assertions.assertEquals("salary", transactionRecord.getPurpose());

        transactionRecord = records.get(5);
        Assertions.assertEquals("2020-01-25", transactionRecord.getValueDate());
        Assertions.assertEquals("TR-47884222203", transactionRecord.getTransID());
        Assertions.assertEquals(new BigDecimal("5000.000"), transactionRecord.getAmount());
        Assertions.assertEquals("JOD", transactionRecord.getCurrency());
        Assertions.assertEquals("not specified", transactionRecord.getPurpose());

        transactionRecord = records.get(6);
        Assertions.assertEquals("2020-01-12", transactionRecord.getValueDate());
        Assertions.assertEquals("TR-47884222245", transactionRecord.getTransID());
        Assertions.assertEquals(new BigDecimal("420.00"), transactionRecord.getAmount());
        Assertions.assertEquals("USD", transactionRecord.getCurrency());
        Assertions.assertEquals("loan", transactionRecord.getPurpose());
    }

    @Test
    public void givenNullPath_whenRaed_thenThrowException() {
        List<TransactionRecord> records = new ArrayList<>();

        NullPointerException exception = assertThrows(NullPointerException.class,
                () -> new JSONTransactionFileReader().read(null, records::add));

        assertEquals("path is null", exception.getMessage());
    }

    @Test
    public void givenInvalidFileName_whenValidating_thenThrowException() {
        Path invalidFileName = Paths.get(".", "jip9.json");
        List<TransactionRecord> records = new ArrayList<>();

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> new JSONTransactionFileReader().read(invalidFileName, records::add));
        assertEquals("path does not exist", exception.getMessage());
    }

    @Test
    public void givenInvalidDirectory_whenValidating_thenThrowException() {
        Path directory = Paths.get(".");
        List<TransactionRecord> records = new ArrayList<>();

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> new JSONTransactionFileReader().read(directory, records::add));
        assertEquals("path is not a file", exception.getMessage());
    }
}