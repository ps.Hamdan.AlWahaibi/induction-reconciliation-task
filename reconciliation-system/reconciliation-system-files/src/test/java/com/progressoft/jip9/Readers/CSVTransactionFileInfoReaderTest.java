package com.progressoft.jip9.Readers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CSVTransactionFileInfoReaderTest {

    @Test
    public void canCreate() {
        new CSVTransactionFileReader();
    }

    // happy scenario only
    @Test
    public void givenValidPath_whenRead_thenSuccess() throws IOException {
        Path path = Files.createTempFile("transaction", ".csv");
        try (InputStream resourceAsStream =
                     this.getClass().getResourceAsStream("/bank-transactions.csv");
             OutputStream outputStream = Files.newOutputStream(path)) {
            int read;
            while ((read = resourceAsStream.read()) != -1)
                outputStream.write(read);
        }

        CSVTransactionFileReader reader = new CSVTransactionFileReader();
        List<TransactionRecord> records = new ArrayList<>();
        reader.read(path, records::add);

        Assertions.assertNotNull(records, "records are null");
        Assertions.assertEquals(records.size(), 6);

        TransactionRecord transactionRecord = records.get(0);
        Assertions.assertNotNull(transactionRecord);

        Assertions.assertEquals("TR-47884222201", transactionRecord.getTransID());
        Assertions.assertEquals("online transfer", transactionRecord.getTransDesc());
        Assertions.assertEquals(new BigDecimal("140.00"), transactionRecord.getAmount());
        Assertions.assertEquals("USD", transactionRecord.getCurrency());
        Assertions.assertEquals("donation", transactionRecord.getPurpose());
        Assertions.assertEquals("2020-01-20", transactionRecord.getValueDate());
        Assertions.assertEquals(TransType.DEBIT, transactionRecord.getTransType());

        transactionRecord = records.get(1);
        Assertions.assertEquals("TR-47884222202", transactionRecord.getTransID());
        Assertions.assertEquals("atm withdrwal", transactionRecord.getTransDesc());
        Assertions.assertEquals(new BigDecimal("20.000"), transactionRecord.getAmount());
        Assertions.assertEquals("JOD", transactionRecord.getCurrency());
        Assertions.assertEquals("", transactionRecord.getPurpose());
        Assertions.assertEquals("2020-01-22", transactionRecord.getValueDate());
        Assertions.assertEquals(TransType.DEBIT, transactionRecord.getTransType());

        transactionRecord = records.get(2);
        Assertions.assertEquals("TR-47884222203", transactionRecord.getTransID());
        Assertions.assertEquals("counter withdrawal", transactionRecord.getTransDesc());
        Assertions.assertEquals(new BigDecimal("5000.000"), transactionRecord.getAmount());
        Assertions.assertEquals("JOD", transactionRecord.getCurrency());
        Assertions.assertEquals("", transactionRecord.getPurpose());
        Assertions.assertEquals("2020-01-25", transactionRecord.getValueDate());
        Assertions.assertEquals(TransType.DEBIT, transactionRecord.getTransType());

        transactionRecord = records.get(3);
        Assertions.assertEquals("TR-47884222204", transactionRecord.getTransID());
        Assertions.assertEquals("salary", transactionRecord.getTransDesc());
        Assertions.assertEquals(new BigDecimal("1200.000"), transactionRecord.getAmount());
        Assertions.assertEquals("JOD", transactionRecord.getCurrency());
        Assertions.assertEquals("donation", transactionRecord.getPurpose());
        Assertions.assertEquals("2020-01-31", transactionRecord.getValueDate());
        Assertions.assertEquals(TransType.CREDIT, transactionRecord.getTransType());

        transactionRecord = records.get(4);
        Assertions.assertEquals("TR-47884222205", transactionRecord.getTransID());
        Assertions.assertEquals("atm withdrwal", transactionRecord.getTransDesc());
        Assertions.assertEquals(new BigDecimal("60.000"), transactionRecord.getAmount());
        Assertions.assertEquals("JOD", transactionRecord.getCurrency());
        Assertions.assertEquals("", transactionRecord.getPurpose());
        Assertions.assertEquals("2020-02-02", transactionRecord.getValueDate());
        Assertions.assertEquals(TransType.DEBIT, transactionRecord.getTransType());

        transactionRecord = records.get(5);
        Assertions.assertEquals("TR-47884222206", transactionRecord.getTransID());
        Assertions.assertEquals("atm withdrwal", transactionRecord.getTransDesc());
        Assertions.assertEquals(new BigDecimal("500.00"), transactionRecord.getAmount());
        Assertions.assertEquals("USD", transactionRecord.getCurrency());
        Assertions.assertEquals("", transactionRecord.getPurpose());
        Assertions.assertEquals("2020-02-10", transactionRecord.getValueDate());
        Assertions.assertEquals(TransType.DEBIT, transactionRecord.getTransType());
    }

    @Test
    public void givenNullPath_whenRaed_thenThrowException() {
        List<TransactionRecord> records = new ArrayList<>();

        NullPointerException exception = assertThrows(NullPointerException.class,
                () -> new CSVTransactionFileReader().read(null, records::add));

        assertEquals("path is null", exception.getMessage());
    }

    @Test
    public void givenInvalidFileName_whenValidating_thenThrowException() {
        Path invalidFileName = Paths.get(".", "jip9.csv");
        List<TransactionRecord> records = new ArrayList<>();

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> new CSVTransactionFileReader().read(invalidFileName, records::add));
        assertEquals("path does not exist", exception.getMessage());
    }

    @Test
    public void givenInvalidDirectory_whenValidating_thenThrowException() {
        Path directory = Paths.get(".");
        List<TransactionRecord> records = new ArrayList<>();

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> new CSVTransactionFileReader().read(directory, records::add));
        assertEquals("path is not a file", exception.getMessage());
    }
}
