package com.progressoft.jip9.Comparator;

import com.progressoft.jip9.Exceptions.JSONParseException;
import com.progressoft.jip9.Readers.TransactionRecord;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

public class FilesComparatorTest {
    @Test
    public void canCreate() {
        new FilesComparator();
    }

    @Test
    public void givenTwoValidFiles_whenCompare_thenSuccess() throws IOException, JSONParseException {
        Path sourcePath = Files.createTempFile("transaction", ".csv");
        try (InputStream resourceAsStream =
                     this.getClass().getResourceAsStream("/bank-transactions.csv");
             OutputStream outputStream = Files.newOutputStream(sourcePath)) {
            int read;
            while ((read = resourceAsStream.read()) != -1)
                outputStream.write(read);
        }

        Path targetPath = Files.createTempFile("transaction", ".JSON");
        try (InputStream resourceAsStream =
                     this.getClass().getResourceAsStream("/online-banking-transactions.json");
             OutputStream outputStream = Files.newOutputStream(targetPath)) {
            int read;
            while ((read = resourceAsStream.read()) != -1)
                outputStream.write(read);
        }
        FileInfo source = new FileInfo(sourcePath, FileType.CSV);
        FileInfo target = new FileInfo(targetPath, FileType.JSON);

        FilesComparator comparator = new FilesComparator();
        List<Map<String, TransactionRecord>> listOfComparisons = comparator.compare(source, target);


        Map<String, TransactionRecord> matchingRecord = listOfComparisons.get(0);
        Map<String, TransactionRecord> missMatchingRecord = listOfComparisons.get(1);
        Map<String, TransactionRecord> missingRecord = listOfComparisons.get(2);

        Assertions.assertEquals(3, matchingRecord.size());

        TransactionRecord record = matchingRecord.get("BOTH0");
        Assertions.assertEquals("TR-47884222201", record.getTransID());
        Assertions.assertEquals(new BigDecimal("140.00"), record.getAmount());
        Assertions.assertEquals("USD", record.getCurrency());
        Assertions.assertEquals("2020-01-20", record.getValueDate());

        record = matchingRecord.get("BOTH1");
        Assertions.assertEquals("TR-47884222203", record.getTransID());
        Assertions.assertEquals(new BigDecimal("5000.000"), record.getAmount());
        Assertions.assertEquals("JOD", record.getCurrency());
        Assertions.assertEquals("2020-01-25", record.getValueDate());

        record = matchingRecord.get("BOTH2");
        Assertions.assertEquals("TR-47884222206", record.getTransID());
        Assertions.assertEquals(new BigDecimal("500.00"), record.getAmount());
        Assertions.assertEquals("USD", record.getCurrency());
        Assertions.assertEquals("2020-02-10", record.getValueDate());

        Assertions.assertEquals(4, missMatchingRecord.size());

        record = missMatchingRecord.get("SOURCE0");
        Assertions.assertEquals("TR-47884222202", record.getTransID());
        Assertions.assertEquals(new BigDecimal("20.000"), record.getAmount());
        Assertions.assertEquals("JOD", record.getCurrency());
        Assertions.assertEquals("2020-01-22", record.getValueDate());

        record = missMatchingRecord.get("TARGET0");
        Assertions.assertEquals("TR-47884222202", record.getTransID());
        Assertions.assertEquals(new BigDecimal("30.000"), record.getAmount());
        Assertions.assertEquals("JOD", record.getCurrency());
        Assertions.assertEquals("2020-01-22", record.getValueDate());

        record = missMatchingRecord.get("SOURCE1");
        Assertions.assertEquals("TR-47884222205", record.getTransID());
        Assertions.assertEquals(new BigDecimal("60.000"), record.getAmount());
        Assertions.assertEquals("JOD", record.getCurrency());
        Assertions.assertEquals("2020-02-02", record.getValueDate());

        record = missMatchingRecord.get("TARGET1");
        Assertions.assertEquals("TR-47884222205", record.getTransID());
        Assertions.assertEquals(new BigDecimal("60.000"), record.getAmount());
        Assertions.assertEquals("JOD", record.getCurrency());
        Assertions.assertEquals("2020-02-03", record.getValueDate());

        Assertions.assertEquals(3, missingRecord.size());

        record = missingRecord.get("SOURCE0");
        Assertions.assertEquals("TR-47884222204", record.getTransID());
        Assertions.assertEquals(new BigDecimal("1200.000"), record.getAmount());
        Assertions.assertEquals("JOD", record.getCurrency());
        Assertions.assertEquals("2020-01-31", record.getValueDate());

        record = missingRecord.get("TARGET1");
        Assertions.assertEquals("TR-47884222217", record.getTransID());
        Assertions.assertEquals(new BigDecimal("12000.000"), record.getAmount());
        Assertions.assertEquals("JOD", record.getCurrency());
        Assertions.assertEquals("2020-02-14", record.getValueDate());

        record = missingRecord.get("TARGET2");
        Assertions.assertEquals("TR-47884222245", record.getTransID());
        Assertions.assertEquals(new BigDecimal("420.00"), record.getAmount());
        Assertions.assertEquals("USD", record.getCurrency());
        Assertions.assertEquals("2020-01-12", record.getValueDate());

        //  you should have more assertions for the record contents
    }
}
