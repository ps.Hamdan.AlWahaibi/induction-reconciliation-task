package com.progressoft.jip9.Comparator;

import com.progressoft.jip9.Exceptions.FileException;
import com.progressoft.jip9.Exceptions.JSONParseException;
import com.progressoft.jip9.Readers.CSVTransactionFileReader;
import com.progressoft.jip9.Readers.JSONTransactionFileReader;
import com.progressoft.jip9.Readers.TransactionFileReader;
import com.progressoft.jip9.Readers.TransactionRecord;

import java.nio.file.Path;
import java.util.*;

public class FilesComparator {

    //  introduce parameter object for those parameters, more than 4 parameters
    public List<Map<String, TransactionRecord>> compare(FileInfo source, FileInfo target)
            throws FileException, JSONParseException {

        List<Map<String, TransactionRecord>> listOfComparisons = new LinkedList<>();

        List<TransactionRecord> sourceRecords = new LinkedList<>();
        List<TransactionRecord> targetRecords = new LinkedList<>();

        useSuitableReader(source.getPath(), source.getFileType(), sourceRecords);
        useSuitableReader(target.getPath(), target.getFileType(), targetRecords);

        compareLists(sourceRecords, targetRecords, listOfComparisons);

        return listOfComparisons;
    }

    private void compareLists(List<TransactionRecord> sourceRecords, List<TransactionRecord> targetRecords,
                              List<Map<String, TransactionRecord>> listOfComparisons) {
        List<TransactionRecord> tempSrc = new LinkedList<>(sourceRecords);
        List<TransactionRecord> tempTarget = new LinkedList<>(targetRecords);

        Map<String, TransactionRecord> matchingRecord = new LinkedHashMap<>();
        Map<String, TransactionRecord> missMatchingRecord = new LinkedHashMap<>();
        Map<String, TransactionRecord> missingRecord = new LinkedHashMap<>();

        int matchedCount = 0;
        int missMatchedCount = 0;
        int missingCount = 0;

        for (TransactionRecord sourceRecord : sourceRecords) {
            for (TransactionRecord targetRecord : targetRecords) {
                if (sourceRecord.getTransID().equals(targetRecord.getTransID())) {
                    //  you can encapsulate the behavior of comparing in a method
                    if (isEqual(sourceRecord, targetRecord)) {
                        matchingRecord.put("BOTH" + matchedCount, sourceRecord);
                        matchedCount++;
                    } else {
                        missMatchingRecord.put("SOURCE" + missMatchedCount, sourceRecord);
                        missMatchingRecord.put("TARGET" + missMatchedCount, targetRecord);
                        missMatchedCount++;
                    }
                    tempSrc.remove(sourceRecord);
                    tempTarget.remove(targetRecord);
                }
            }

        }
        for (TransactionRecord missingSrc : tempSrc) {
            missingRecord.put("SOURCE" + missingCount, missingSrc);
            missingCount++;
        }
        for (TransactionRecord missingTarget : tempTarget) {
            missingRecord.put("TARGET" + missingCount, missingTarget);
            missingCount++;

        }

        listOfComparisons.add(matchingRecord);
        listOfComparisons.add(missMatchingRecord);
        listOfComparisons.add(missingRecord);
    }

    private boolean isEqual(TransactionRecord sourceRecord, TransactionRecord targetRecord) {
        return sourceRecord.getAmount().equals(targetRecord.getAmount()) &&
                sourceRecord.getCurrency().equals(targetRecord.getCurrency()) &&
                sourceRecord.getValueDate().equals(targetRecord.getValueDate());
    }


    private void useSuitableReader(Path path, FileType type, List<TransactionRecord> records) throws FileException, JSONParseException {
        TransactionFileReader reader;
        switch (type) {
            case CSV:
                reader = new CSVTransactionFileReader();
                reader.read(path, records::add);
                break;
            case JSON:
                reader = new JSONTransactionFileReader();
                reader.read(path, records::add);
                break;
        }
    }
}
