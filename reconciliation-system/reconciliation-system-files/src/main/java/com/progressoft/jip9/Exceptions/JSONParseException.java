package com.progressoft.jip9.Exceptions;

import java.text.ParseException;

public class JSONParseException extends ParseException {
    public JSONParseException(String message, int parsePosition) {
        super(message, parsePosition);
    }
}
