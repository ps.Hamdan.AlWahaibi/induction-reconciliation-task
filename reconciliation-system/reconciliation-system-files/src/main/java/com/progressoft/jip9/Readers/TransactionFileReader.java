package com.progressoft.jip9.Readers;

import com.progressoft.jip9.Exceptions.FileException;
import com.progressoft.jip9.Exceptions.JSONParseException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Consumer;

public interface TransactionFileReader {
    void read(Path path, Consumer<TransactionRecord> consumer) throws FileException, JSONParseException;

    default void pathValidation(Path path) {
        if (path == null)
            throw new NullPointerException("path is null");
        if (Files.notExists(path))
            throw new IllegalArgumentException("path does not exist");
        if (Files.isDirectory(path))
            throw new IllegalArgumentException("path is not a file");
    }
}
