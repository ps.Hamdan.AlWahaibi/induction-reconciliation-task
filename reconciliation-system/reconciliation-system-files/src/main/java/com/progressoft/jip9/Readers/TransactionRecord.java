package com.progressoft.jip9.Readers;

import java.math.BigDecimal;

public interface TransactionRecord {
    String getTransID();

    String getTransDesc();

    BigDecimal getAmount();

    String getCurrency();

    String getPurpose();

    String getValueDate();

    TransType getTransType();
}
