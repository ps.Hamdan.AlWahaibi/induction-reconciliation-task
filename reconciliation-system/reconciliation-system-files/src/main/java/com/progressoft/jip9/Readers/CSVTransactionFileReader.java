package com.progressoft.jip9.Readers;

import com.progressoft.jip9.Exceptions.FileException;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Currency;
import java.util.Objects;
import java.util.function.Consumer;

public class CSVTransactionFileReader implements TransactionFileReader {

    @Override
    public void read(Path path, Consumer<TransactionRecord> consumer) throws FileException {

        pathValidation(path);

        try (BufferedReader reader = Files.newBufferedReader(path)) {
            readRecords(reader, (consumer));
        } catch (IOException e) {
            throw new FileException("can not open the file");
            //  what is this, this is not how we handle exception, throwing then swallowing at once?!!!
        }

    }

    private void readRecords(BufferedReader reader, Consumer<TransactionRecord> consumer) throws IOException {
        String line = reader.readLine();
        if (line == null)
            return;
        while ((line = reader.readLine()) != null) {
            TransactionRecord TransactionRecord = readLine(line);
            consumer.accept(TransactionRecord);
        }
    }

    private CSVTransactionRecord readLine(String line) {
        String[] fields = line.split(",", -1);
        Currency currency = Currency.getInstance(fields[3]);
        int defaultFractionalDigits = currency.getDefaultFractionDigits();
        BigDecimal amount = new BigDecimal(fields[2])
                .setScale(defaultFractionalDigits, BigDecimal.ROUND_HALF_UP);
        return new CSVTransactionRecord()
                .setTransID(fields[0])
                .setTransDesc(fields[1])
                .setAmount(amount)
                .setCurrency(fields[3])
                .setPurpose(fields[4])
                .setValueDate(fields[5])
                .setTransType(TransType.byCode(fields[6]));
    }

    private class CSVTransactionRecord implements TransactionRecord {

        private String transID;
        private String transDesc;
        private BigDecimal amount;
        private String currency;
        private String purpose;
        private String valueDate;
        TransType transType;

        public CSVTransactionRecord setTransID(String transID) {
            this.transID = transID;
            return this;
        }

        public CSVTransactionRecord setTransDesc(String transDesc) {
            this.transDesc = transDesc;
            return this;

        }

        public CSVTransactionRecord setAmount(BigDecimal amount) {
            this.amount = amount;
            return this;
        }

        public CSVTransactionRecord setCurrency(String currency) {
            this.currency = currency;
            return this;
        }

        public CSVTransactionRecord setPurpose(String purpose) {
            this.purpose = purpose;
            return this;
        }

        public CSVTransactionRecord setValueDate(String valueDate) {
            this.valueDate = valueDate;
            return this;
        }

        public CSVTransactionRecord setTransType(TransType transType) {
            this.transType = transType;
            return this;
        }


        @Override
        public String getTransID() {
            return transID;
        }

        @Override
        public String getTransDesc() {
            return transDesc;
        }

        @Override
        public BigDecimal getAmount() {
            return amount;
        }

        @Override
        public String getCurrency() {
            return currency;
        }

        @Override
        public String getPurpose() {
            return purpose;
        }

        @Override
        public String getValueDate() {
            return valueDate;
        }

        @Override
        public TransType getTransType() {
            return transType;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            CSVTransactionRecord that = (CSVTransactionRecord) o;
            return Objects.equals(transID, that.transID) &&
                    amount.compareTo(that.amount) == 0 &&
                    Objects.equals(currency, that.currency) &&
                    Objects.equals(valueDate, that.valueDate);

        }

        @Override
        public int hashCode() {
            return Objects.hash(transID, transDesc, amount, currency, purpose, valueDate, transType);
        }
    }
}
