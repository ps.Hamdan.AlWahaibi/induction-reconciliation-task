package com.progressoft.jip9.Readers;

public enum TransType {
    DEBIT("D"), CREDIT("C");

    private final String shortName;

    TransType(String shortName) {
        this.shortName = shortName;
    }

    public static TransType byCode(String shortName) {
        switch (shortName) {
            case "D":
                return DEBIT;
            case "C":
                return CREDIT;
        }
        throw new IllegalArgumentException("Unknown code: " + shortName);
    }
}

