package com.progressoft.jip9.Exporter;

import com.progressoft.jip9.Exceptions.FileException;
import com.progressoft.jip9.Readers.TransactionRecord;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

public class ResultFilesExporter {
    public void export(List<Map<String, TransactionRecord>> listOfComparisons) {
        if (listOfComparisons == null)
            throw new NullPointerException("list is null");
        Map<String, TransactionRecord> matchedRecords = listOfComparisons.get(0);
        Map<String, TransactionRecord> missMatchedRecords = listOfComparisons.get(1);
        Map<String, TransactionRecord> missingRecords = listOfComparisons.get(2);


        write(matchedRecords, ResultFile.MATCHED);
        write(missMatchedRecords, ResultFile.MISMATCHED);
        write(missingRecords, ResultFile.MISSING);

    }

    private void write(Map<String, TransactionRecord> records,
                       ResultFile resultFile) {
        String path = getPath(resultFile);
        try (PrintWriter printWriter = new PrintWriter(
                new File(path))) {
            printFileLocation(resultFile, printWriter, "found in file");
            printWriter.append("transaction id");
            printWriter.append(",");
            printWriter.append("amount");
            printWriter.append(",");
            printWriter.append("currecny code");
            printWriter.append(",");
            printWriter.append("value date");
            printWriter.append("\n");

            int rows = 0;

            for (Map.Entry<String, TransactionRecord> record : records.entrySet()) {
                String fileLocation = record.getKey().replaceAll("([0-9])", "");
                printFileLocation(resultFile, printWriter, fileLocation);
                printWriter.append(record.getValue().getTransID());
                printWriter.append(",");
                printWriter.append(record.getValue().getAmount().toString());
                printWriter.append(",");
                printWriter.append(record.getValue().getCurrency());
                printWriter.append(",");
                printWriter.append(record.getValue().getValueDate());
                if (rows >= records.size() - 1)
                    break;
                printWriter.append("\n");
                rows++;
            }
        } catch (IOException e) {
            try {
                throw new FileException("can not open the file");
            } catch (FileException fileException) {
                fileException.printStackTrace();
            }

        }
    }

    private void printFileLocation(ResultFile resultFile, PrintWriter printWriter, String fileLocation) {
        if (resultFile != ResultFile.MATCHED) {
            printWriter.append(fileLocation);
            printWriter.append(",");
        }
    }

    private String getPath(ResultFile resultFile) {
        String path;
        switch (resultFile) {
            case MATCHED:
                path = "matching-transactions.csv";
                break;
            case MISSING:
                path = "missing-transactions.csv";
                break;
            case MISMATCHED:
                path = "mismatching-transactions.csv";
                break;
            default:
                throw new IllegalArgumentException("invalid type");
        }
        return path;
    }
}

