package com.progressoft.jip9.Comparator;

import java.nio.file.Path;

public class FileInfo {
    private final Path path;
    private final FileType fileType;

    public FileInfo(Path path, FileType fileType) {
        this.path = path;
        this.fileType = fileType;
    }


    public Path getPath() {
        return path;
    }

    public FileType getFileType() {
        return fileType;
    }
}
