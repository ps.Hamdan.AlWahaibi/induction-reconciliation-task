package com.progressoft.jip9.Readers;

import com.progressoft.jip9.Exceptions.FileException;
import com.progressoft.jip9.Exceptions.JSONParseException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParsePosition;
import java.util.Currency;
import java.util.function.Consumer;

public class JSONTransactionFileReader implements TransactionFileReader {

    @Override
    public void read(Path path, Consumer<TransactionRecord> consumer) throws JSONParseException, FileException {
        pathValidation(path);
        JSONParser parser = new JSONParser();
        try (BufferedReader reader = Files.newBufferedReader(path)) {
            JSONArray records = (JSONArray) parser.parse(reader);
            for (Object record : records) {
                JSONObject jsonObject = (JSONObject) record;
                TransactionRecord TransactionRecord = readObject(jsonObject);
                consumer.accept(TransactionRecord);
            }

        } catch (ParseException e) {
            ParsePosition position = new ParsePosition(0);
            throw new JSONParseException("wrong file format", position.getErrorIndex());
            // why handling exception in this way ya Hamdan?!!!
        } catch (IOException e) {
            throw new FileException("can not open the file");
        }

    }

    private JSONTransactionRecord readObject(JSONObject jsonObject) {
        Currency currency = Currency.getInstance((String) jsonObject.get("currencyCode"));
        int defaultFractionalDigits = currency.getDefaultFractionDigits();
        BigDecimal amount = new BigDecimal((String) jsonObject.get("amount"))
                .setScale(defaultFractionalDigits, BigDecimal.ROUND_HALF_UP);

        String[] dateArr = ((String) jsonObject.get("date")).split("/");
        String date = dateArr[2] + "-" + dateArr[1] + "-" + dateArr[0];


        return new JSONTransactionRecord().
                setValueDate(date)
                .setTransID((String) jsonObject.get("reference"))
                .setAmount(amount)
                .setCurrency((String) jsonObject.get("currencyCode"))
                .setPurpose((String) jsonObject.get("purpose"));
    }

    private static class JSONTransactionRecord implements TransactionRecord {

        private String transID;
        private BigDecimal amount;
        private String currency;
        private String purpose;
        private String valueDate;

        private JSONTransactionRecord() {
        }

        public JSONTransactionFileReader.JSONTransactionRecord setTransID(String transID) {
            this.transID = transID;
            return this;
        }

        public JSONTransactionFileReader.JSONTransactionRecord setAmount(BigDecimal amount) {
            this.amount = amount;
            return this;
        }

        public JSONTransactionFileReader.JSONTransactionRecord setCurrency(String currency) {
            this.currency = currency;
            return this;
        }

        public JSONTransactionFileReader.JSONTransactionRecord setPurpose(String purpose) {
            this.purpose = purpose;
            return this;
        }

        public JSONTransactionFileReader.JSONTransactionRecord setValueDate(String valueDate) {
            this.valueDate = valueDate;
            return this;
        }


        @Override
        public String getTransID() {
            return transID;
        }

        @Override
        public String getTransDesc() {
            return null;
        }

        @Override
        public BigDecimal getAmount() {
            return amount;
        }

        @Override
        public String getCurrency() {
            return currency;
        }

        @Override
        public String getPurpose() {
            return purpose;
        }

        @Override
        public String getValueDate() {
            return valueDate;
        }

        @Override
        public TransType getTransType() {
            return null;
        }


    }
}
