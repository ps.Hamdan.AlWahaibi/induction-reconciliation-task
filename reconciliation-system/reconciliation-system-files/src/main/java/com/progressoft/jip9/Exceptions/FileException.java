package com.progressoft.jip9.Exceptions;

import java.io.IOException;

public class FileException extends IOException {
    public FileException(String message) {
        super(message);
    }
}
