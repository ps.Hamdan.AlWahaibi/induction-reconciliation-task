package com.progressoft.jip9.Exporter;

public enum ResultFile {
    MATCHED, MISMATCHED, MISSING
}
