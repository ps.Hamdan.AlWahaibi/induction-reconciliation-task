package com.progressoft.jip9;
import com.mysql.cj.jdbc.MysqlDataSource;
import java.sql.*;
public class UserDAO {
    public User getUserByEmail(String email, String password) throws SQLException,
            ClassNotFoundException {
        final String MYSQL_URL = "jdbc:mysql://localhost:3306/reconciliation_system?useSSL=false";
        final String USERNAME = "root";
        final String PASSWORD = "root";
        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setURL(MYSQL_URL);
        dataSource.setUser(USERNAME);
        dataSource.setPassword(PASSWORD);
        User user;
        try (Connection connection = dataSource.getConnection()) {
            System.out.println("try");
            String sql = "SELECT * FROM users WHERE email = ? and password = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, email);
            statement.setString(2, password);
            ResultSet result = statement.executeQuery();
            user = null;
            if (result.next()) {
                user = new User();
                user.setUsername(email);
            }
            connection.close();
        }
        return user;
    }
}